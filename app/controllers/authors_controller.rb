class AuthorsController < ApplicationController
  before_action :set_author, only: %i[show update destroy]

  def index
    authors = Author.order('created_at DESC')
    render json: authors
  end

  def show
    render json: @author || 'Couldn\'t find author', status: @author ? :ok : :not_found
  end

  def create
    @author = Author.new(author_params)
    if @author.save
      render json: @author, status: :created
    else
      render json: 'Validation failed: Created by can\'t be blank', status: :unprocessable_entity
    end
  end

  def update
    @author.update(author_params)
    head :no_content
  end

  def destroy
    @author.destroy
    head :no_content
  end

  private

  def author_params
    params.require(:author).permit(:name, :email, :birthdate)
  end

  def set_author
    @author = Author.find(params[:id])
  end
end
