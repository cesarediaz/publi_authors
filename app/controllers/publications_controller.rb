class PublicationsController < ApplicationController
  before_action :set_author, only: %i[index show create update destroy]

  def index
    publications = @author.publications
    render json: publications
  end

  def show
    @publication = @author.publications.where(id: params[:id])&.first
    render json: @publication || 'Couldn\'t find publication', status: @publication ? :ok : :not_found
  end

  def create
    publication = @author.publications.new(publication_params)
    if publication.save
      render json: publication, status: :created
    else
      render json: 'Validation failed: Created by can\'t be blank', status: :unprocessable_entity
    end
  end

  def update
    @publication = @author.publications.where(id: params[:id]).first
    @publication.update(publication_params)
    head :no_content
  end

  def destroy
    @publication = @author.publications.where(id: params[:id]).first
    @publication.destroy
    head :no_content
  end

  private

  def publication_params
    params.require(:publication).permit(:title, :body, :published_at)
  end

  def set_author
    @author = Author.find(params[:author_id])
  end
end
