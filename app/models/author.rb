class Author
  include Mongoid::Document
  field :name, type: String
  field :email, type: String
  field :birthdate, type: Date
  embeds_many :publications

  # validations
  validates :name, :email, :birthdate, presence: true
end
