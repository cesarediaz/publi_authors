class Publication
  include Mongoid::Document
  field :title, type: String
  field :body, type: String
  field :published_at, type: Date
  embedded_in :author

  # validations
  validates :title, :body, :published_at, presence: true
end
