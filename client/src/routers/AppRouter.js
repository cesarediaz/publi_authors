import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NotFoundPage from '../components/NotFoundPage';
import DashboardPage from '../components/DashboarPage';
import AddAuthorPage from '../components/AddAuthorPage';
import AddAuthorPublicationPage from '../components/AddAuthorPublicationPage';
import EditAuthorPage from '../components/EditAuthorPage';
import Header from '../components/Header';

const AppRouter = () => (
  <Router>
    <div>
      <Switch>
        <Route path='/' component={DashboardPage} exact={true} />
        <Route path='/authors/new' component={AddAuthorPage} />
        <Route path='/authors/:id/edit' component={EditAuthorPage} />
        <Route path='/authors/:author_id/add_publication' component={AddAuthorPublicationPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;
