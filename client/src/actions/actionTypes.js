export const ADD_AUTHOR = 'ADD_AUTHOR';
export const EDIT_AUTHOR = 'EDIT_AUTHOR';
export const GET_AUTHOR = 'GET_AUTHOR';
export const GET_AUTHORS = 'GET_AUTHORS';
export const REMOVE_AUTHOR = 'REMOVE_AUTHOR';
export const ADD_PUBLICATION = 'ADD_PUBLICATION';
