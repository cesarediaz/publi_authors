import {  ADD_PUBLICATION
        } from './actionTypes';
import axios from 'axios';

// ADD_PUBLICATION
export const addPublication = (publication) => ({
  type: ADD_PUBLICATION,
  publication: publication
})

// START_ADD_AUTHOR_PUBLICATION
export const startAddAuthorPublication = (title, body, published_at, author_id) => {

  const publication = {
    title: title,
    body: body,
    published_at: published_at
  };

  return (dispatch) => {
    return axios.post(`/api/v1/authors/${author_id}/publications`, publication)
    .then(response => {
      dispatch(addPublication({_id: {'$oid': response.data._id['$oid']}, ...publication}))
    })
    .catch(error => {
      throw(error);
    });
  }
}
