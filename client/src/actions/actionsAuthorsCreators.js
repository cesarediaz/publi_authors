import {  ADD_AUTHOR,
          EDIT_AUTHOR,
          GET_AUTHOR,
          GET_AUTHORS,
          REMOVE_AUTHOR
        } from './actionTypes';
import axios from 'axios';

const authorsApiUrl = '/api/v1/authors';

// REMOVE AUTHOR
export const removeAuthor = (id) => ({
  type: REMOVE_AUTHOR,
  id
});

// START_REMOVE_AUTHOR
export const startRemoveAuthor = (id) => {
  return (dispatch) => {
    return axios.delete(`/api/v1/authors/${id}`)
    .then(response => {
      dispatch(removeAuthor(id));
    })
    .catch(error => console.log(error))
  }
}

// ADD_AUTHOR
export const addAuthor = (author) => ({
  type: ADD_AUTHOR,
  author: author
})

// START_ADD_AUTHOR
export const startAddAuthor = (name, email, birthdate) => {
  const author = {
    name: name,
    email: email,
    birthdate: birthdate
  };

  return (dispatch) => {
    return axios.post(authorsApiUrl, author)
    .then(response => {
      dispatch(addAuthor({_id: {'$oid': response.data._id['$oid']}, ...author}))
    })
    .catch(error => {
      throw(error);
    });
  }
}

// EDIT_AUTHOR
export const editAuthor = (data) => ({
  type: EDIT_AUTHOR,
  id: data.id,
  author: data.author
})

// START_EDIT_AUTHOR
export const startEditAuthor = (id, updates) => {
  const author = {
    author: {
      id: id,
      name: updates.name,
      email: updates.email,
      birthdate: updates.birthdate
    }
  };

  return (dispatch) => {
    return axios.put(`/api/v1/authors/${id}`, author)
      .then(response => {
        dispatch(editAuthor({id: response.data.id, ...author}))
      })
      .catch(error => console.log(error))
    };
};

// GET_AUTHOR
export const getAuthor = (author) => ({
  type: GET_AUTHOR,
  author
})

// GET_AUTHORS
export const getAuthors = (authors) => {
  return {
    type: GET_AUTHORS,
    authors
  }
};

// SET_GET_AUTHORS
export const startGetAuthors = () => {
  let authors = [];
  return (dispatch) => {
    return axios.get(authorsApiUrl)
      .then(response => {
        response.data.forEach((author) => {
          authors.push(author);
        });
        dispatch(getAuthors(authors))
      })
      .catch(error => {
        throw(error);
      });
  };
};
