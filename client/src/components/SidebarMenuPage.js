import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

class SidebarMenuPage extends Component {
  render() {
    return (
      <div>
        <nav id="sidebar">
          <div class="sidebar-header">
              <h3>Authors & Publications</h3>
          </div>
          <NavLink  to='/authors/new'
              exact={true}
              className='btn btn-sm btn-warning'>
              New Author
          </NavLink>


          <ul class="list-unstyled components">
            {this.props.authors.map((author) => {
              return(
                <li className="active" key={author._id['$oid']}>
                  <div className="row col-12">
                    <div>
                      {author.name}
                    </div>
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <Link to={`/authors/${author._id['$oid']}/edit`} className='pull-left btn-warning'>
                        Edit
                      </Link>
                      <Link to={`/authors/${author._id['$oid']}/add_publication`} className='pull-left btn-warning'>
                        Add Publication
                      </Link>
                    </div>
                  </div>
                </li>
                )
              })
            }
          </ul>
        </nav>
      </div>
    )
  }
}

export default SidebarMenuPage;
