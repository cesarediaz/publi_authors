import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startGetAuthors, startRemoveAuthor } from '../actions/actionsAuthorsCreators';
import SidebarMenuPage from './SidebarMenuPage';

class DashboardPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authors: props.authors ? props.authors : []
    }
  }

  onRemove = (e) => {
    this.props.startRemoveAuthor(e.target.id);
  }

  render() {
    return (
      <div>
        <div class="wrapper">
          <SidebarMenuPage authors={this.props.authors} />
          <div id="content">
            <nav>
              <div class="container-fluid">
              </div>
            </nav>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  authors: state.authors
});

const mapDispatchToProps = (dispatch) => ({
  startGetAuthors: dispatch(startGetAuthors()),
  startRemoveAuthor: (id) => dispatch(startRemoveAuthor(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);
