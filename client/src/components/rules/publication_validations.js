const NOT_EMPTY = 'should not be empty';
const MINIMUM_LENGTH = 'should have a minimum length of characters';

export const RULES = {
  fields: {
    title: {
      message: `${NOT_EMPTY} and ${MINIMUM_LENGTH}`,
      minimum_length: 10
    },
    body: {
      message: `${NOT_EMPTY} and ${MINIMUM_LENGTH}`,
      minimum_length: 20
    }
  }
};
