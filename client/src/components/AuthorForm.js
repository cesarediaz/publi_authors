import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import $ from 'jquery';
import { RULES } from './rules/author_validations';

export default class AuthorForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id ? props.id : undefined,
      name: props.author ? props.author.name : '',
      email: props.author ? props.author.email : '',
      startDate: props.author ? new Date(this.props.author.birthdate) : new Date(moment().subtract(1, "days")),
      formErrors: {name: '', email:''},
      formValidity: {name: false, email: false},
      isSubmitDisabled: true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFieldsChange = this.handleFieldsChange.bind(this);
  }

  componentDidMount() {
    this.validateField('name', this.state.name);
    this.validateField('email', this.state.email);
    console.log('name ', this.state.name);
    console.log('email ', this.state.email);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({id: nextProps.author._id['$oid'] })
    this.setState({name: nextProps.author.name })
    this.setState({email: nextProps.author.email })
    this.setState({startDate: new Date(nextProps.author.birthdate) })
  }

  handleFieldsChange(e){
    let  name = e.target.name
    let  value = e.target.value
    this.setState({
      [name]: value,
    }, function(){ this.validateField(name, value)})
  }

  handleChange(date) {
    this.setState({startDate: date});
  }

  validateField(name, value) {
    const fieldValidationErrors = this.state.formErrors
    const validity = this.state.formValidity
    const rule = RULES.fields[`${name}`];

    validity[name] = value && value.length >= rule.minimum_length

    fieldValidationErrors[name] = validity[name] ? '': `"${name}" ${rule.message} : ${rule.minimum_length}`;
    this.setState({
      formErrors: fieldValidationErrors,
      formValidity: validity,
    }, () => this.canSubmit())
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'is-invalid');
  }

  canSubmit() {
    let valid = this.state.formValidity.name && this.state.formValidity.email ? false : true;
    this.setState({ isSubmitDisabled: valid });
  }

  onSubmit = (e) => {
    e.preventDefault();
    let name = $('#name').val();
    let email = $('#email').val();
    let birthdate = $('#birthdate').val();

    if (this.state.id  === undefined) {
      this.props.startAddAuthor(name, email, birthdate)
    } else {
      this.props.startEditAuthor(this.state.id['$oid'], {name, email, birthdate})
    }

    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <div className="container">
          {this.state.error &&
            <div className="alert alert-danger" role="alert">
              {this.state.error}
            </div>
          }
          <div className="row">
            <div className="col-md-12 col-md-offset-3">
              <form onSubmit={this.onSubmit} action={this.props.action} id={this.props.id}>
                <div className="row">
                  <label htmlFor="name" className="col-sm-6 col-form-label">Name<small>(*)</small></label>
                  <div className="col-sm-12 form-group">
                    <input  type="text"
                            name="name"
                            placeholder="Name"
                            maxLength="50"
                            id="name"
                            onChange={this.handleFieldsChange}
                            value={this.state.name}
                            className={`form-control ${this.errorClass(this.state.formErrors.name)}`}
                            />
                     <div className="invalid-feedback">{this.state.formErrors.name}</div>
                  </div>
                </div>

                <div className="row">
                  <label htmlFor="email" className="col-sm-6 col-form-label">Email<small>(*)</small></label>
                  <div className="col-sm-12 form-group">
                    <input  type="text"
                            name="email"
                            placeholder="Email"
                            maxLength="50"
                            id="email"
                            className={`form-control ${this.errorClass(this.state.formErrors.email)}`}
                            value={this.state.email}
                            onChange={this.handleFieldsChange}
                            />
                    <div className="invalid-feedback">{this.state.formErrors.email}</div>
                  </div>
                </div>

                <div className="row">
                  <label htmlFor="birthdate" className="col-sm-6 col-form-label">Birthdate</label>
                  <div className="col-sm-12 form-group">
                    <DatePicker
                      id="birthdate"
                      name="birthdate"
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      value={this.state.birthdate}
                      filterDate={(date) => {
                        return moment().subtract(100, "years") < date && moment().subtract(1, "days") > date;
                      }}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm- form-group">
                    <button className="btn btn-secondary btn-block"
                            disabled={this.state.isSubmitDisabled}>
                            Submit
                    </button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
