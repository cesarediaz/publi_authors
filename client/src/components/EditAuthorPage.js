import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { startEditAuthor } from '../actions/actionsAuthorsCreators';
import AuthorForm from "./AuthorForm";
import SidebarMenuPage from './SidebarMenuPage';

class EditAuthorPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authors: props.authors ? props.authors : []
    }
  }

  render() {
    return (
      <div>
        <div class="wrapper">
          <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fas fa-align-justify"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">
                  <Link  to='/'
                    exact={true}
                    className='btn btn-sm btn-outline-primary'>
                    Dashboard
                  </Link>
                </a>
              </li>
            </ul>
          </div>
          <SidebarMenuPage authors={this.props.authors} />
          <div id="content">
            <nav>
              <div class="container-fluid">
              <AuthorForm history={this.props.history}
                          startEditAuthor={this.props.startEditAuthor}
                          author={this.props.author}
                          action='update'
                          id={this.props.author._id} />

              </div>
            </nav>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => ({
  author: state.authors.find((author) => author._id['$oid'] === props.match.params.id),
  authors: state.authors
})

const mapDispatchToProps = (dispatch) => ({
  startEditAuthor: (id, {name, address, birthdate}) => dispatch(startEditAuthor(id, {name, address, birthdate})),
})

export default connect(mapStateToProps, mapDispatchToProps)(EditAuthorPage);
