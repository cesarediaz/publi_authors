import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import $ from 'jquery';
import { RULES } from './rules/publication_validations';

export default class PublicationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id ? props.id : undefined,
      title: props.publication ? props.publication.title : '',
      body: props.publication ? props.publication.body : '',
      startDate: props.publication ? new Date(this.props.publication.published_at) : new Date(moment().subtract(1, "days")),
      formErrors: {title: '', body:''},
      formValidity: {title: false, body: false},
      isSubmitDisabled: true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFieldsChange = this.handleFieldsChange.bind(this);
  }

  componentDidMount() {
    this.validateField('title', this.state.title);
    this.validateField('body', this.state.body);
  }

  handleFieldsChange(e){
    let  name = e.target.name;
    let  value = e.target.value;

    this.setState({
      [name]: value,
    }, function(){ this.validateField(name, value)})
  }
  handleChange(date) {
    console.log('date ->', date);
    this.setState({startDate: date});

  }

  validateField(title, value) {
    const fieldValidationErrors = this.state.formErrors
    const validity = this.state.formValidity
    const rule = RULES.fields[`${title}`];

    if (rule !== undefined) {
      validity[title] = value && value.length >= rule.minimum_length

      fieldValidationErrors[title] = validity[title] ? '': `"${title}" ${rule.message} : ${rule.minimum_length}`;
      this.setState({
        formErrors: fieldValidationErrors,
        formValidity: validity,
      }, () => this.canSubmit())
    }
  }

  errorClass(error) {
    return(error.length === 0 ? '' : 'is-invalid');
  }

  canSubmit() {
    let valid = this.state.formValidity.title && this.state.formValidity.body ? false : true;
    this.setState({ isSubmitDisabled: valid });
  }

  onSubmit = (e) => {
    e.preventDefault();
    let author_id = window.location.pathname.split('/')[2];
    let title = $('#title').val();
    let body = $('#body').val();
    let published_at = $('#published_at').val();

    if (this.state.id  === undefined) {
      this.props.startAddAuthorPublication(title, body, published_at, author_id)
    } else {
      this.props.startEditAuthor(this.state.id['$oid'], {title, body, published_at})
    }

    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <div className="container">
          {this.state.error &&
            <div className="alert alert-danger" role="alert">
              {this.state.error}
            </div>
          }
          <div className="row">
            <div className="col-md-12 col-md-offset-3">
              <form onSubmit={this.onSubmit} action={this.props.action} id={this.props.id}>
                <div className="row">
                  <label htmlFor="title" className="col-sm-6 col-form-label">Title<small>(*)</small></label>
                  <div className="col-sm-12 form-group">
                    <input  type="text"
                            name="title"
                            placeholder="Title"
                            maxLength="50"
                            id="title"
                            onChange={this.handleFieldsChange}
                            value={this.state.title}
                            className={`form-control ${this.errorClass(this.state.formErrors.title)}`}
                            />
                     <div className="invalid-feedback">{this.state.formErrors.title}</div>
                  </div>
                </div>

                <div className="row">
                  <label htmlFor="body" className="col-sm-6 col-form-label">Body<small>(*)</small></label>
                  <div className="col-sm-12 form-group">
                    <input  type="text"
                            name="body"
                            placeholder="Body"
                            maxLength="50"
                            id="body"
                            className={`form-control ${this.errorClass(this.state.formErrors.body)}`}
                            onChange={this.handleFieldsChange}
                            value={this.state.body}
                            />
                    <div className="invalid-feedback">{this.state.formErrors.body}</div>
                  </div>
                </div>

                <div className="row">
                  <label htmlFor="published_at" className="col-sm-6 col-form-label">Published At</label>
                  <div className="col-sm-12 form-group">
                    <DatePicker
                      id="published_at"
                      name="published_at"
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      value={this.state.published_at}
                      filterDate={(date) => {
                        return moment().subtract(100, "years") < date && moment().subtract(1, "days") > date;
                      }}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm- form-group">
                    <button className="btn btn-secondary btn-block"
                            disabled={this.state.isSubmitDisabled}>
                            Submit
                    </button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
