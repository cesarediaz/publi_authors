import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { startAddAuthorPublication } from '../actions/actionsAuthorPlublicationsCreators';
import PublicationForm from "./PublicationForm";
import SidebarMenuPage from './SidebarMenuPage';

class AddAuthorPublicationPage extends Component {
  render() {
    return (
      <div>
        <div class="wrapper">
          <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fas fa-align-justify"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">
                  <Link  to='/'
                    exact={true}
                    className='btn btn-sm btn-outline-primary'>
                    Dashboard
                  </Link>
                </a>
              </li>
            </ul>
          </div>
          <SidebarMenuPage authors={this.props.authors} />
          <div id="content">
            <nav>
              <div class="container-fluid">
                <PublicationForm history={this.props.history}
                  startAddAuthorPublication={this.props.startAddAuthorPublication}
                  action='create'
                />
              </div>
            </nav>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => ({
  authors: state.authors
})


const mapDispatchToProps = (dispatch) => ({
  startAddAuthorPublication: (title, body, published_at, author_id) => dispatch(startAddAuthorPublication(title, body, published_at, author_id))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddAuthorPublicationPage);
