import {ADD_PUBLICATION
} from '../actions/actionTypes';

function publicationsReducer(state = [], action)
{
switch(action.type) {
  case ADD_PUBLICATION:
    return [
      ...state,
      action.publication
    ];
  default:
    return state;
  }
}

export default publicationsReducer;
