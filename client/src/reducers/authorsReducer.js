import {  GET_AUTHORS,
          ADD_AUTHOR,
          REMOVE_AUTHOR
        } from '../actions/actionTypes';

function authorsReducer(state = [], action)
{
  switch(action.type) {
    case GET_AUTHORS:
      return action.authors;
    case ADD_AUTHOR:
      return [
          ...state,
          action.author
      ];
    case REMOVE_AUTHOR:
      return state.filter(({ _id }) => _id['$oid'] !== action.id);
    default:
      return state;
  }
}

export default authorsReducer;
