import { combineReducers } from 'redux'
import authorsReducer from './authorsReducer'
import publicationsReducer from './publicationsReducer'

const rootReducer = combineReducers({
  authors: authorsReducer,
  publications: publicationsReducer
});

export default rootReducer;
