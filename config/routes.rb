Rails.application.routes.draw do
  scope '/api/v1' do
    resources :authors do
      resources :publications
    end
  end
end
