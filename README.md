# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

###  Ruby version
  ```
  ruby '2.4.1'
  ```

###  System dependencies
  ```
  * node: 10.15.2
  ```

  ```
  * yarn: 1.7.0
  ```

  ```
  * MongoDB: 3.6.8
  ```
### Configuration
  ```
  * bundle install
  ```

  ```
  * yarn install
  ```
###  Database creation
  Not necessary

###  Database initialization
  Not necessary

###  How to run the test suite
  ```
  bundle exec rspec
  ```
###  Start application locally
  ```
  rake start:development
  ```

###  Services (job queues, cache servers, search engines, etc.)

###  Deployment instructions

### * ...
