FactoryBot.define do
  factory :author do
    name { Faker::Book.author }
    email { Faker::Internet.email }
    birthdate { Faker::Date.birthday(18, 65) }
    publications {[FactoryBot.build(:publication_1), FactoryBot.build(:publication_2)] }
  end



  factory :publication_1, class: :publication do
    title { Faker::Book.title }
    body { Faker::Quote.matz }
    plublished_at { Faker::Time.backward(rand(20_000), :morning) }
  end

  factory :publication_2, class: :publication do
    title { Faker::Book.title }
    body { Faker::Quote.matz }
    plublished_at { Faker::Time.backward(rand(20_000), :evening) }
  end
end
