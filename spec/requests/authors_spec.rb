require 'rails_helper'

RSpec.describe 'Author API', type: :request do
  # initialize test data
  let!(:authors) { create_list(:author, 10) }
  let(:author_id) { authors.first.id.to_s }

  # Test suite for GET /authors
  describe 'GET /api/v1/authors' do
    # make HTTP get request before each example
    before { get '/api/v1/authors' }

    it 'returns authors' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /authors/:id
  describe 'GET /api/v1/authors/:id' do
    before { get "/api/v1/authors/#{author_id}" }

    context 'when the record exists' do
      it 'returns the author' do
        expect(json).not_to be_empty
        expect(json['_id']["$oid"]).to eq(author_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:author_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find author/)
      end
    end
  end

  # Test suite for POST /authors
  describe 'POST /api/v1/authors' do
    # valid payload
    let(:name) { Faker::Book.author }
    let(:valid_attributes) { { author: { name: name, email: Faker::Internet.email,  birthdate: Faker::Date.birthday(18, 65) } } }

    context 'when the request is valid' do
      before { post '/api/v1/authors', params: valid_attributes }

      it 'creates a author' do
        expect(json['name']).to eq(name)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/authors', params: { author: { name: Faker::Book.author } } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Created by can't be blank/)
      end
    end
  end

  # Test suite for PUT /authors/:id
  describe 'PUT /api/v1/authors/:id' do
    let(:valid_attributes) { { author: { title: 'Shopping' } } }

    context 'when the record exists' do
      before { put "/api/v1/authors/#{author_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /authors/:id
  describe 'DELETE /api/v1/authors/:id' do
    before { delete "/api/v1/authors/#{author_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
