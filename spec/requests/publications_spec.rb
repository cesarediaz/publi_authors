require 'rails_helper'

RSpec.describe 'Publication API', type: :request do
  # initialize test data
  let!(:author_publications) do
    create(:author,
           publications: [FactoryBot.build(:publication_1),
                          FactoryBot.build(:publication_2)])
  end
  let(:author_id) { author_publications.id.to_s }
  let(:publication_id) { author_publications.publications.first.id.to_s }

  # Test suite for GET /publications
  describe 'GET /api/v1/authors/:author_id/publications' do
    # make HTTP get request before each example
    before { get "/api/v1/authors/#{author_id}/publications" }

    it 'returns publications' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(2)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /api/v1/authors/:author_id/publications/:id
  describe 'GET /api/v1/authors/:author_id/publications/:id' do
    before { get "/api/v1/authors/#{author_id}/publications/#{publication_id}" }

    context 'when the record exists' do
      it 'returns the publication' do
        expect(json).not_to be_empty
        expect(json['_id']['$oid']).to eq(publication_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:publication_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find publication/)
      end
    end
  end

  # Test suite for POST /publications
  describe 'POST /api/v1/authors/:author_id/publications' do
    # valid payload
    let(:title) { Faker::Book.title }
    let(:valid_attributes) do
      { publication:
        {  title: title,
           body: Faker::Lorem.sentence,
           plublished_at: Faker::Time.between(2.days.ago, Date.today, :morning) } }
    end

    context 'when the request is valid' do
      before do
        post "/api/v1/authors/#{author_id}/publications", \
             params: valid_attributes
      end

      it 'creates a publication' do
        expect(json['title']).to eq(title)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before do
        post "/api/v1/authors/#{author_id}/publications", \
             params: { publication: { title: title } }
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Created by can't be blank/)
      end
    end
  end

  # Test suite for PUT /api/v1/authors/:author_id/publications/:id
  describe 'PUT /api/v1/authors/:author_id/publications/:id' do
    let(:valid_attributes) { { publication: { title: 'Shopping' } } }

    context 'when the record exists' do
      before do
        put "/api/v1/authors/#{author_id}/publications/#{publication_id}", \
            params: valid_attributes
      end

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /api/v1/authors/:author_id/publications/:id
  describe 'DELETE /api/v1/authors/:author_id/publications/:id' do
    before do
      delete "/api/v1/authors/#{author_id}/publications/#{publication_id}"
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
