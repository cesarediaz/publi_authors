require 'rails_helper'

RSpec.describe Publication, type: :model do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:body) }
  it { is_expected.to validate_presence_of(:plublished_at) }

  it { is_expected.to be_embedded_in(:author) }
end
